# インストールした discord.py を読み込む
import discord

import numpy as np
import pandas as pd
from pulp import *
import ast
from platforms import *
from ranks import *
from ortoolpy import addvar, addvars, addbinvars

from auth import *
from platforms import *
from ranks import *
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

# 自分のBotのアクセストークンに置き換えてください
TOKEN = 'NTg3MTQzMTI2MTU4NjcxODcy.XPyRtA.G1MIAAcl7xbcE1O288Gu1k8I8xI'
auth = Auth("aka.sute.san@gmail.com", "Daisuke123")

# 接続に必要なオブジェクトを生成
client = discord.Client()

cred = credentials.Certificate("./discordr6s.json")
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://discordr6s-4f464.firebaseio.com/',
    'databaseAuthVariableOverride': {
        'uid': 'my-service-worker'
    }
})

ref = db.reference('/user')

sco = ""


# 起動時に動作する処理
@client.event
async def on_ready():
    # 起動したらターミナルにログイン通知が表示される
    print('ログインしました')


# メッセージ受信時に動作する処理
@client.event
async def on_message(message):
    # メッセージ送信者がBotだった場合は無視する
    if message.author.bot:
        return

    if message.content.startswith('!rank'):
        name = message.content[len('!rank'):].strip()
        if name == "":
            UID = ref.child('udiscord').child(str(message.author.id)).child("uplay-id").get()
            if UID != "":
                try:
                    player = await auth.get_player(uid=UID, platform=Platforms.UPLAY)
                    rank = await player.get_rank(RankedRegions.ASIA)
                    await player.load_queues()
                    embed = discord.Embed(title="ランク戦績",
                                          description="Year4 Season1 : Operation Burnt Horizon \n リージョン: Asia ",
                                          color=0x3fcada)
                    embed.set_author(name=player.name, url=player.url, icon_url=player.icon_url)
                    embed.set_thumbnail(url=rank.get_icon_url())
                    embed.add_field(name="キル数", value=player.ranked.kills, inline=True)
                    embed.add_field(name="デス数", value=player.ranked.deaths, inline=True)
                    embed.add_field(name="キルレ", value=round(player.ranked.kills / player.ranked.deaths, 2), inline=True)
                    embed.add_field(name="勝利数", value=player.ranked.wins, inline=True)
                    embed.add_field(name="敗北数", value=player.ranked.losses, inline=True)
                    embed.add_field(name="勝利率",
                                    value=round(player.ranked.wins / (player.ranked.losses + player.ranked.wins), 2),
                                    inline=True)
                    embed.add_field(name="現在のランク", value=rank.rank, inline=True)
                    embed.add_field(name="最高のランク", value=Rank.get_max_rank(rank), inline=True)
                    embed.add_field(name="現在のMMR", value=round(rank.mmr, 2), inline=True)
                    embed.add_field(name="次のランクのMMR", value=round(rank.next_rank_mmr, 2), inline=True)
                    embed.add_field(name="最高のMMR", value=round(rank.max_mmr, 2), inline=True)
                    embed.add_field(name="プレイ時間", value=player.ranked.time_played, inline=True)

                    await message.channel.send(embed=embed)
                except:
                    await message.channel.send("情報を取得できませんでした")
            else:
                await message.channel.send("先に !set <UPlay ID> でアカウント登録を行ってください ")
        else:
            try:
                player = await auth.get_player(name, Platforms.UPLAY)
                rank = await player.get_rank(RankedRegions.ASIA)
                await player.load_queues()
                embed = discord.Embed(title="ランク戦績",
                                      description="Year4 Season1 : Operation Burnt Horizon \n リージョン: Asia ",
                                      color=0x3fcada)
                embed.set_author(name=player.name, url=player.url, icon_url=player.icon_url)
                embed.set_thumbnail(url=rank.get_maxicon_url())
                embed.add_field(name="キル数", value=player.ranked.kills, inline=True)
                embed.add_field(name="デス数", value=player.ranked.deaths, inline=True)
                embed.add_field(name="キルレ", value=round(player.ranked.kills / player.ranked.deaths, 2), inline=True)
                embed.add_field(name="勝利数", value=player.ranked.wins, inline=True)
                embed.add_field(name="敗北数", value=player.ranked.losses, inline=True)
                embed.add_field(name="勝利率",
                                value=round(player.ranked.wins / (player.ranked.losses + player.ranked.wins), 2),
                                inline=True)
                embed.add_field(name="現在のランク", value=rank.rank, inline=True)
                embed.add_field(name="最高のランク", value=Rank.get_max_rank(rank), inline=True)
                embed.add_field(name="現在のMMR", value=round(rank.mmr, 2), inline=True)
                embed.add_field(name="次のランクのMMR", value=round(rank.next_rank_mmr, 2), inline=True)
                embed.add_field(name="最高のMMR", value=round(rank.max_mmr, 2), inline=True)
                embed.add_field(name="プレイ時間", value=player.ranked.time_played, inline=True)

                await message.channel.send(embed=embed)
            except:
                await message.channel.send("情報を取得できませんでした")

    if message.content.startswith('!set'):
        name = message.content[len('!set'):].strip()
        player = await auth.get_player(name, Platforms.UPLAY)
        discord_ref = ref.child('udiscord')
        discord_ref.update({
            str(message.author.id) + "/uplay-id": player.id
        })
        uplay_ref = ref.child('uplay')
        uplay_ref.update({
            player.id + "/discord-id": message.author.id
        })

        await message.channel.send(
            "Discordアカウント " + str(message.author) + " をUplay(APAC)アカウント " + player.name + " と連携しました。")

    if message.content.startswith("!team"):
        name = message.content[len('!team'):].strip()
        if name.startswith("generate"):
            voicechannel = client.get_channel(585043002653802507)
            members = voicechannel.members
            await message.channel.send(await setTeam(members))

        if name.startswith("debug"):
            ids = ref.child('uplay').get()
            uids = []
            for id in ids:
                uids.append(id)

            await message.channel.send(await setDTeam(uids))


    if message.content.startswith("!debug"):
        name = message.content[len('!debug'):].strip()
        player = await auth.get_player(name, Platforms.UPLAY)
        """
        rank = await player.get_rank(RankedRegions.ASIA)
        await player.load_queues()
        kd = round(player.ranked.kills / player.ranked.deaths, 2)"""
        await message.channel.send(str(len(player.id)) + "  :  " + player.id)

    if message.content.startswith("!match"):
        name = message.content[len("!match"):].strip()
        if name.startswith("end"):
            orangec = client.get_channel(584712490454351903)
            bluec = client.get_channel(584712528249094154)
            taikic = client.get_channel(585043002653802507)
            for mem in orangec.members:
                await mem.move_to(taikic)
            for mem in bluec.members:
                await mem.move_to(taikic)

    if message.content.startswith('!add'):
        uplay_id = message.content[5:41]
        discord_id = message.content[42:60]

        discord_ref = ref.child('udiscord')
        discord_ref.update({
            discord_id + "/uplay-id": uplay_id
        })
        uplay_ref = ref.child('uplay')
        uplay_ref.update({
            uplay_id + "/discord-id": discord_id
        })

        await message.channel.send(
            "DiscordID " + str(discord_id) + " をUplay(APAC)アカウント " + uplay_id + " と連携しました。")



async def setDTeam(uids):
    teams = ['ブルーチーム', 'オレンジチーム']
    skills = ['Now MMR', 'Max MMR', 'K/D Ratio']
    playernames = []
    scdata = []
    for uid in uids:
        uplayer = await auth.get_player(uid=uid,
                                        platform=Platforms.UPLAY)
        rank = await uplayer.get_rank(RankedRegions.ASIA)
        await uplayer.load_queues()
        playernames.append(uplayer.name)
        scdata.append([rank.mmr, rank.max_mmr, uplayer.ranked.kills / uplayer.ranked.deaths])
    scores = pd.DataFrame(data=scdata,
                          index=playernames,
                          columns=skills)
    nteams = len(teams)  # チーム数
    nmembers = len(scores.index)  # メンバー数
    nskills = len(scores.columns)  # 能力種別数

    m = LpProblem()  # 数理モデル
    x = pd.DataFrame(addbinvars(nmembers, nteams), index=playernames, columns=teams)  # 割当
    y = pd.DataFrame(addvars(nteams, nskills), index=teams, columns=skills)  # チーム内の平均偏差
    mu = pd.DataFrame(addvars(nteams), index=teams)  # チーム内の平均
    z = pd.DataFrame(addvars(nteams), index=teams)  # チームごとの平均偏差
    nu = addvar()  # 全チームの平均

    m.setObjective(lpSum([lpSum(y.loc[j]) + 1.5 * z.loc[j] for j in teams]))  # 目的関数

    m.addConstraint(lpSum(np.dot(scores.sum(1), x)) / nteams == nu)
    for j in teams:
        m.addConstraint(lpDot(scores.sum(1), x[j]) - nu <= z.loc[j])
        m.addConstraint(lpDot(scores.sum(1), x[j]) - nu >= -z.loc[j])
        m.addConstraint(lpSum(np.dot(x[j], scores)) / nskills == mu.loc[j])
        for k in skills:
            m.addConstraint(lpDot(scores[k], x[j]) - mu.loc[j] <= y.loc[j, k])
            m.addConstraint(lpDot(scores[k], x[j]) - mu.loc[j] >= -y.loc[j, k])
    for i in playernames:
        m.addConstraint(lpSum(x.loc[i]) == 1)  # どこかのチームに所属

    m.solve()  # 求解

    if m.status:
        message = "```チームの振り分けが終了しました！\nブルーチーム:\n"
        for i, xi in enumerate(np.vectorize(value)(x).tolist()):
            if teams[xi.index(1)] == "ブルーチーム":
                message += playernames[i] + "\n"
        message += "オレンジチーム:\n"
        for i, xi in enumerate(np.vectorize(value)(x).tolist()):
            if teams[xi.index(1)] == "オレンジチーム":
                message += playernames[i]  + "\n"
        message += "MMR および KDからの算出です。```"
        message += "```" + str(scores) + "```"

    else:
        message = "エラー: 最適解の発見に失敗しました"

    return message


async def setTeam(members):
    teams = ['ブルーチーム', 'オレンジチーム']
    skills = ['Now MMR', 'Max MMR', 'K/D Ratio']
    playernames = []
    scdata = []
    for member in members:
        uplayer = await auth.get_player(uid=ref.child('udiscord').child(str(member.id)).child("uplay-id").get(),
                                        platform=Platforms.UPLAY)
        rank = await uplayer.get_rank(RankedRegions.ASIA)
        await uplayer.load_queues()
        playernames.append(uplayer.name)
        scdata.append([rank.mmr, rank.max_mmr, uplayer.ranked.kills / uplayer.ranked.deaths])
    scores = pd.DataFrame(data=scdata,
                          index=playernames,
                          columns=skills)
    nteams = len(teams)  # チーム数
    nmembers = len(scores.index)  # メンバー数
    nskills = len(scores.columns)  # 能力種別数

    m = LpProblem()  # 数理モデル
    x = pd.DataFrame(addbinvars(nmembers, nteams), index=members, columns=teams)  # 割当
    y = pd.DataFrame(addvars(nteams, nskills), index=teams, columns=skills)  # チーム内の平均偏差
    mu = pd.DataFrame(addvars(nteams), index=teams)  # チーム内の平均
    z = pd.DataFrame(addvars(nteams), index=teams)  # チームごとの平均偏差
    nu = addvar()  # 全チームの平均

    m.setObjective(lpSum([lpSum(y.loc[j]) + 1.5 * z.loc[j] for j in teams]))  # 目的関数

    m.addConstraint(lpSum(np.dot(scores.sum(1), x)) / nteams == nu)
    for j in teams:
        m.addConstraint(lpDot(scores.sum(1), x[j]) - nu <= z.loc[j])
        m.addConstraint(lpDot(scores.sum(1), x[j]) - nu >= -z.loc[j])
        m.addConstraint(lpSum(np.dot(x[j], scores)) / nskills == mu.loc[j])
        for k in skills:
            m.addConstraint(lpDot(scores[k], x[j]) - mu.loc[j] <= y.loc[j, k])
            m.addConstraint(lpDot(scores[k], x[j]) - mu.loc[j] >= -y.loc[j, k])
    for i in playernames:
        m.addConstraint(lpSum(x.loc[i]) == 1)  # どこかのチームに所属

    m.solve()  # 求解

    if m.status:
        message = "```チームの振り分けが終了しました！\nブルーチーム:\n"
        for i, xi in enumerate(np.vectorize(value)(x).tolist()):
            if teams[xi.index(1)] == "ブルーチーム":
                message += playernames[i] + "\n"
        message += "オレンジチーム:\n"
        for i, xi in enumerate(np.vectorize(value)(x).tolist()):
            if teams[xi.index(1)] == "オレンジチーム":
                message += playernames[i] + "\n"
        message += "MMR および KDからの算出です。```"
        message += "```" + str(scores) + "```"
    else:
        message = "エラー: 最適解の発見に失敗しました"

    return message


# Botの起動とDiscordサーバーへの接続
client.run(TOKEN)
