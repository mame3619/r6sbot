class GameQueue:
    """ゲームレベル

    内訳
    ----------
    name : str
        ゲームレベル名 casual か　rankedが入る
    won : int
        そのゲームレベルでの勝利数
    lost : int
        そのゲームレベルでの敗北数
    time_played : int
        そのゲームレベルでのプレイ時間
    played : int
        そのゲームレベルでのプレイ回数
    kills : int
        そのゲームレベルでのキル数
    deaths : int
        そのゲームレベルでのデス数"""
    def __init__(self, name, stats=None):
        self.name = name

        statname = name + "pvp_"

        stats = stats or {}
        self.won = stats.get(statname + "matchwon", 0)
        self.lost = stats.get(statname + "matchlost", 0)
        self.time_played = stats.get(statname + "timeplayed", 0)
        self.played = stats.get(statname + "matchplayed", 0)
        self.kills = stats.get(statname + "kills", 0)
        self.deaths = stats.get(statname + "death", 0)

    @property
    def wins(self):
        return self.won

    @property
    def losses(self):
        return self.lost