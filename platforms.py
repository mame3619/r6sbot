class Platforms:
    """プラットフォーム

    内容
    ----------
    UPLAY : str
        PC
    XBOX : str
        XBOX
    PLAYSTATION : str
        ぷれすて"""

    UPLAY = "uplay"
    XBOX = "xbl"
    PLAYSTATION = "psn"


valid_platforms = [x.lower() for x in dir(Platforms) if "_" not in x]


PlatformURLNames = {
    "uplay": "OSBOR_PC_LNCH_A",
    "psn": "OSBOR_PS4_LNCH_A",
    "xbl": "OSBOR_XBOXONE_LNCH_A"
}
