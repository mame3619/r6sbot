import asyncio
import inspect
from exceptions import InvalidRequest
from platforms import PlatformURLNames
from weapons import *
from gamemodes import *
from gamequeues import *
from operators import *
from ranks import *


class PlayerUrlTemplates:
    FETCH_STATISTIC = "https://public-ubiservices.ubi.com/v1/spaces/%s/sandboxes/%s/playerstats2/statistics?populations=%s&statistics=%s"
    LOAD_LEVEL = "https://public-ubiservices.ubi.com/v1/spaces/%s/sandboxes/%s/r6playerprofile/playerprofile/progressions?profile_ids=%s"
    LOAD_RANK = "https://public-ubiservices.ubi.com/v1/spaces/%s/sandboxes/%s/r6karma/players?board_id=pvp_ranked&profile_ids=%s&region_id=%s&season_id=%s"
    LOAD_OPERATOR = "https://public-ubiservices.ubi.com/v1/spaces/%s/sandboxes/%s/playerstats2/statistics?populations=%s&statistics=%s"
    LOAD_WEAPON = "https://public-ubiservices.ubi.com/v1/spaces/%s/sandboxes/%s/playerstats2/statistics?populations=%s&statistics=weapontypepvp_kills,weapontypepvp_headshot,weapontypepvp_bulletfired,weapontypepvp_bullethit"


class PlayerUrlBuilder:

    def __init__(self, spaceid, platform_url, player_ids):
        self.spaceid = spaceid
        self.platform_url = platform_url
        self.player_ids = player_ids

        if isinstance(player_ids, list) or isinstance(player_ids, tuple):
            player_ids = ",".join(player_ids)

    def fetch_statistic_url(self, statistics):
        return PlayerUrlTemplates.FETCH_STATISTIC % (self.spaceid, self.platform_url, self.player_ids, ",".join(statistics))

    def load_level_url(self):
        return PlayerUrlTemplates.LOAD_LEVEL % (self.spaceid, self.platform_url, self.player_ids)
    
    def load_rank_url(self, region, season):
        return PlayerUrlTemplates.LOAD_RANK % (self.spaceid, self.platform_url, self.player_ids, region, season)

    def load_operator_url(self, statistics):
        return PlayerUrlTemplates.LOAD_OPERATOR % (self.spaceid, self.platform_url, self.player_ids, statistics)

    def load_weapon_url(self):
        return PlayerUrlTemplates.LOAD_WEAPON % (self.spaceid, self.platform_url, self.player_ids)


class PlayerBatch:

    def __init__(self, players):
        self.players = players
        self.player_ids = [player_id for player_id in players]
        self._player_objs = [players[player_id] for player_id in players]

        if len(players) == 0:
            raise ValueError("batch must contain at least one player")

    def __iter__(self):
        return iter(self._player_objs)

    def __getitem__(self, name):
        return self.players[name]

    def __getattr__(self, name):
        root_player = self.players[self.player_ids[0]]
        root_method = getattr(root_player, name)

        @asyncio.coroutine
        def _proxy(*args, **kwargs):
            results = {}

            # temporarily override url builder so we get data for all players
            root_player.url_builder.player_ids = ",".join(self.player_ids)

            root_result = yield from root_method(*args, **kwargs)
            results[root_player.id] = root_result
            
            data = root_player._last_data
            kwargs["data"] = data

            for player_id in self.players:
                if player_id != root_player.id:
                    results[player_id] = yield from getattr(self.players[player_id], name)(*args, **kwargs)

            # reset root player url builder to default state
            root_player.url_builder.player_ids = root_player.id

            return results
        
        return _proxy

    

class Player:
    """プレイヤー

    内容
    ----------
    auth : :class:`Auth`
        プレイヤーAuth
    id : str
        プロフィールID
    userid : str
        ユーザーID
    platform : str
        プラットフォーム名
    platform_url : str
        プラットフォームURL
    id_on_platform : str
        プラットフォームでのID
    name : str
        プラットフォーム上の名前
    url : str
        公式Statsのページurl
    icon_url : str
        アバターURL
    xp : int
        これまでの獲得XP
    level : int
        現在のレベル
    ranks : dict
        ランクデータ
    operators : dict
        オペレーターデータ
    gamemodes : dict
        ゲームモードデータ
    weapons : dict
        ウェポンデータ
    casual : :class:`GameQueue`
        カジュアルデータ
    ranked : :class:`GameQueue`
        ランクデータ
    deaths : int
        デス数
    kills : int
        キル数
    kill_assists : int
        キルアシスト
    revives : int
        組成
    matches_won : int
        勝利数
    matches_lost : int
        敗北数
    matches_played : int
        マッチ数
    time_played : int
        プレイ時間
    bullets_fired : int
        銃弾発射数
    bullets_hit : int
        銃弾ヒット数
    headshots : int
        ヘッドショット数
    terrorist_hunt : :class:`GameQueue`
        テロハントデータ
    """

    def __init__(self, auth, data):
        self.auth = auth

        self.id = data.get("profileId")
        self.userid = data.get("userId")
        self.platform = data.get("platformType")
        self.platform_url = PlatformURLNames[self.platform]
        self.id_on_platform = data.get("idOnPlatform")
        self.name = data.get("nameOnPlatform")
        self.url_builder = PlayerUrlBuilder(self.spaceid, self.platform_url, self.id)

        self.url = "https://game-rainbow6.ubi.com/en-us/%s/player-statistics/%s/multiplayer" % (self.platform, self.id)
        self.icon_url = "https://ubisoft-avatars.akamaized.net/%s/default_146_146.png" % (self.id)

        self.ranks = {}
        self.operators = {}
        self.gamemodes = {}
        self.weapons = []

        self.casual = None
        self.ranked = None
        self.terrorist_hunt = None

        self._last_data = None

    @property
    def spaceid(self):
        return self.auth.spaceids[self.platform]

    @asyncio.coroutine
    def _fetch_statistics(self, *statistics, data=None):
        if data is None:
            data = yield from self.auth.get(self.url_builder.fetch_statistic_url(statistics))
            self._last_data = data

        if not "results" in data or not self.id in data["results"]:
            raise InvalidRequest("Missing results key in returned JSON object %s" % str(data))

        data = data["results"][self.id]
        stats = {}

        for x in data:
            statistic = x.split(":")[0]
            if statistic in statistics:
                stats[statistic] = data[x]

        return stats

    @asyncio.coroutine
    def load_level(self, data=None):
        """|coro|

        Load the players XP and level"""
        if data is None:
            data = yield from self.auth.get(self.url_builder.load_level_url())
            self._last_data = data

        if "player_profiles" in data and len(data["player_profiles"]) > 0:
            self.xp = data["player_profiles"][0].get("xp", 0)
            self.level = data["player_profiles"][0].get("level", 0)
        else:
            raise InvalidRequest("Missing key player_profiles in returned JSON object %s" % str(data))

    @asyncio.coroutine
    def check_level(self):
        """|coro|

        Check the players XP and level, only loading it if it hasn't been loaded yet"""
        if not hasattr(self, "level"):
            yield from self.load_level()

    @asyncio.coroutine
    def load_rank(self, region, season=-1, data=None):
        """|coro|
        Loads the players rank for this region and season

        Parameters
        ----------
        region : str
            the name of the region you want to get the rank for
        season : Optional[int]
            the season you want to get the rank for (defaults to -1, latest season)

        Returns
        -------
        :class:`Rank`
            the players rank for this region and season"""
        if data is None:
            data = yield from self.auth.get(self.url_builder.load_rank_url(region, season))
            self._last_data = data

        if "players" in data and self.id in data["players"]:
            regionkey = "%s:%s" % (region, season)
            self.ranks[regionkey] = Rank(data["players"][self.id])
            return self.ranks[regionkey]
        else:
            raise InvalidRequest("Missing players key in returned JSON object %s" % str(data))

    @asyncio.coroutine
    def get_rank(self, region, season=-1, data=None):
        """|coro|

        Checks the players rank for this region, only loading it if it hasn't already been found

        Parameters
        ----------
        region : str
            the name of the region you want to get the rank for
        season : Optional[int]
            the season you want to get the rank for (defaults to -1, latest season)

        Returns
        -------
        :class:`Rank`
            the players rank for this region and season"""
        cache_key = "%s:%s" % (region, season)
        if cache_key in self.ranks:
            return self.ranks[cache_key]

        result = yield from self.load_rank(region, season, data=data)
        return result

    @asyncio.coroutine
    def load_all_operators(self, data=None):
        """|coro|

        Loads the player stats for all operators

        Returns
        -------
        dict[:class:`Operator`]
            the dictionary of all operators found"""
        statistics = ",".join(OperatorUrlStatisticNames)

        for operator in OperatorStatisticNames:
            operator_key = yield from self.auth.get_operator_statistic(operator)
            if operator_key:
                statistics += "," + operator_key

        if data is None:
            data = yield from self.auth.get(self.url_builder.load_operator_url(statistics))
            self._last_data = data

        if "results" not in data or not self.id in data["results"]:
            raise InvalidRequest("Missing results key in returned JSON object %s" % str(data))

        data = data["results"][self.id]

        for operator in OperatorStatisticNames:
            location = yield from self.auth.get_operator_index(operator.lower())
            op_data = {x.split(":")[0].split("_")[1]: data[x] for x in data if x is not None and location in x}
            operator_key = yield from self.auth.get_operator_statistic(operator)
            if operator_key:
                op_data["__statistic_name"] = operator_key.split("_")[1]

            self.operators[operator.lower()] = Operator(operator.lower(), op_data)

        return self.operators

    @asyncio.coroutine
    def get_all_operators(self, data=None):
        """|coro|

        Checks the player stats for all operators, loading them all again if any aren't found
        This is significantly more efficient than calling get_operator for every operator name.

        Returns
        -------
        dict[:class:`Operator`]
            the dictionary of all operators found"""
        if len(self.operators) >= len(OperatorStatisticNames):
            return self.operators

        result = yield from self.load_all_operators(data=data)
        return result

    @asyncio.coroutine
    def load_operator(self, operator, data=None):
        """|coro|

        Loads the players stats for the operator

        Parameters
        ----------
        operator : str
            the name of the operator

        Returns
        -------
        :class:`Operator`
            the operator object found"""
        location = yield from self.auth.get_operator_index(operator)
        if location is None:
            raise ValueError("invalid operator %s" % operator)

        operator_key = yield from self.auth.get_operator_statistic(operator)
        if operator_key is not None:
            operator_key = "," + operator_key
        else:
            operator_key = ""

        if data is None:
            statistics = ",".join(OperatorUrlStatisticNames) + operator_key
            data = yield from self.auth.get(self.url_builder.load_operator_url(statistics))
            self._last_data = data

        if not "results" in data or not self.id in data["results"]:
            raise InvalidRequest("Missing results key in returned JSON object %s" % str(data))

        data = data["results"][self.id]

        data = {x.split(":")[0].split("_")[1]: data[x] for x in data if x is not None and location in x}

        if operator_key:
            data["__statistic_name"] = operator_key.split("_")[1]

        #if len(data) < 5:
        #    raise InvalidRequest("invalid number of results for operator in JSON object %s" % data)

        oper = Operator(operator, data)
        self.operators[operator] = oper
        return oper

    @asyncio.coroutine
    def get_operator(self, operator, data=None):
        """|coro|

        Checks the players stats for this operator, only loading them if they haven't already been found

        Parameters
        ----------
        operator : str
            the name of the operator

        Returns
        -------
        :class:`Operator`
            the operator object found"""
        if operator in self.operators:
            return self.operators[operator]

        result = yield from self.load_operator(operator, data=data)
        return result

    @asyncio.coroutine
    def load_weapons(self, data=None):
        """|coro|

        Load the players weapon stats

        Returns
        -------
        list[:class:`Weapon`]
            list of all the weapon objects found"""
        if data is None:
            data = yield from self.auth.get(self.url_builder.load_weapon_url())
            self._last_data = data

        if not "results" in data or not self.id in data["results"]:
            raise InvalidRequest("Missing key results in returned JSON object %s" % str(data))

        data = data["results"][self.id]
        self.weapons = [Weapon(i, data) for i in range(7)]

        return self.weapons

    @asyncio.coroutine
    def check_weapons(self, data=None):
        """|coro|

        Check the players weapon stats, only loading them if they haven't already been found

        Returns
        -------
        list[:class:`Weapon`]
            list of all the weapon objects found"""
        if len(self.weapons) == 0:
            yield from self.load_weapons(data=data)
        return self.weapons

    @asyncio.coroutine
    def load_gamemodes(self, data=None):
        """|coro|

        Loads the players gamemode stats

        Returns
        -------
        dict
            dict of all the gamemodes found (gamemode_name: :class:`Gamemode`)"""

        stats = yield from self._fetch_statistics("secureareapvp_matchwon", "secureareapvp_matchlost", "secureareapvp_matchplayed",
                                                  "secureareapvp_bestscore", "rescuehostagepvp_matchwon", "rescuehostagepvp_matchlost",
                                                  "rescuehostagepvp_matchplayed", "rescuehostagepvp_bestscore", "plantbombpvp_matchwon",
                                                  "plantbombpvp_matchlost", "plantbombpvp_matchplayed", "plantbombpvp_bestscore",
                                                  "generalpvp_servershacked", "generalpvp_serverdefender", "generalpvp_serveraggression",
                                                  "generalpvp_hostagerescue", "generalpvp_hostagedefense", data=data)

        self.gamemodes = {x: Gamemode(x, stats) for x in GamemodeNames}

        return self.gamemodes

    @asyncio.coroutine
    def check_gamemodes(self, data=None):
        """|coro|

        Checks the players gamemode stats, only loading them if they haven't already been found

        Returns
        -------
        dict
            dict of all the gamemodes found (gamemode_name: :class:`Gamemode`)"""
        if len(self.gamemodes) == 0:
            yield from self.load_gamemodes(data=data)
        return self.gamemodes

    @asyncio.coroutine
    def load_general(self, data=None):
        """|coro|

        Loads the players general stats"""

        stats = yield from self._fetch_statistics("generalpvp_timeplayed", "generalpvp_matchplayed", "generalpvp_matchwon",
                                                  "generalpvp_matchlost", "generalpvp_kills", "generalpvp_death",
                                                  "generalpvp_bullethit", "generalpvp_bulletfired", "generalpvp_killassists",
                                                  "generalpvp_revive", "generalpvp_headshot", "generalpvp_penetrationkills",
                                                  "generalpvp_meleekills", "generalpvp_dbnoassists", "generalpvp_suicide",
                                                  "generalpvp_barricadedeployed", "generalpvp_reinforcementdeploy", "generalpvp_totalxp",
                                                  "generalpvp_rappelbreach", "generalpvp_distancetravelled", "generalpvp_revivedenied",
                                                  "generalpvp_dbno", "generalpvp_gadgetdestroy", "generalpvp_blindkills", data=data)

        statname = "generalpvp_"
        self.deaths = stats.get(statname + "death", 0)
        self.penetration_kills = stats.get(statname + "penetrationkills", 0)
        self.matches_won = stats.get(statname + "matchwon", 0)
        self.bullets_hit = stats.get(statname + "bullethit", 0)
        self.melee_kills = stats.get(statname + "meleekills", 0)
        self.bullets_fired = stats.get(statname + "bulletfired", 0)
        self.matches_played = stats.get(statname + "matchplayed", 0)
        self.kill_assists = stats.get(statname + "killassists", 0)
        self.time_played = stats.get(statname + "timeplayed", 0)
        self.revives = stats.get(statname + "revive", 0)
        self.kills = stats.get(statname + "kills", 0)
        self.headshots = stats.get(statname + "headshot", 0)
        self.matches_lost = stats.get(statname + "matchlost", 0)
        self.dbno_assists = stats.get(statname + "dbnoassists", 0)
        self.suicides = stats.get(statname + "suicide", 0)
        self.barricades_deployed = stats.get(statname + "barricadedeployed", 0)
        self.reinforcements_deployed = stats.get(statname + "reinforcementdeploy", 0)
        self.total_xp = stats.get(statname + "totalxp", 0)
        self.rappel_breaches = stats.get(statname + "rappelbreach", 0)
        self.distance_travelled = stats.get(statname + "distancetravelled", 0)
        self.revives_denied = stats.get(statname + "revivedenied", 0)
        self.dbnos = stats.get(statname + "dbno", 0)
        self.gadgets_destroyed = stats.get(statname + "gadgetdestroy", 0)
        self.blind_kills = stats.get(statname + "blindkills")


    @asyncio.coroutine
    def check_general(self, data=None):
        """|coro|

        Checks the players general stats, only loading them if they haven't already been found"""
        if not hasattr(self, "kills"):
            yield from self.load_general(data=data)

    @asyncio.coroutine
    def load_queues(self, data=None):
        """|coro|

        Loads the players game queues"""

        stats = yield from self._fetch_statistics("casualpvp_matchwon", "casualpvp_matchlost", "casualpvp_timeplayed",
                                                  "casualpvp_matchplayed", "casualpvp_kills", "casualpvp_death",
                                                  "rankedpvp_matchwon", "rankedpvp_matchlost", "rankedpvp_timeplayed",
                                                  "rankedpvp_matchplayed", "rankedpvp_kills", "rankedpvp_death", data=data)

        self.ranked = GameQueue("ranked", stats)
        self.casual = GameQueue("casual", stats)


    @asyncio.coroutine
    def check_queues(self, data=None):
        """|coro|

        Checks the players game queues, only loading them if they haven't already been found"""
        if self.casual is None:
            """yield from self.load_queues(data=data)"""
            return True
        else:
            return False

    @asyncio.coroutine
    def load_terrohunt(self, data=None):
        """|coro|

        Loads the player's general stats for terrorist hunt"""
        stats = yield from self._fetch_statistics("generalpve_dbnoassists", "generalpve_death", "generalpve_revive",
                                                  "generalpve_matchwon", "generalpve_suicide", "generalpve_servershacked",
                                                  "generalpve_serverdefender", "generalpve_barricadedeployed", "generalpve_reinforcementdeploy",
                                                  "generalpve_kills", "generalpve_hostagedefense", "generalpve_bulletfired",
                                                  "generalpve_matchlost", "generalpve_killassists", "generalpve_totalxp",
                                                  "generalpve_hostagerescue", "generalpve_penetrationkills", "generalpve_meleekills",
                                                  "generalpve_rappelbreach", "generalpve_distancetravelled", "generalpve_matchplayed",
                                                  "generalpve_serveraggression", "generalpve_timeplayed", "generalpve_revivedenied",
                                                  "generalpve_dbno", "generalpve_bullethit", "generalpve_blindkills", "generalpve_headshot",
                                                  "generalpve_gadgetdestroy", "generalpve_accuracy", data=data)

        self.terrorist_hunt = GameQueue("terrohunt")

        statname = "generalpve_"
        self.terrorist_hunt.deaths = stats.get(statname + "death", 0)
        self.terrorist_hunt.penetration_kills = stats.get(statname + "penetrationkills", 0)
        self.terrorist_hunt.matches_won = stats.get(statname + "matchwon", 0)
        self.terrorist_hunt.bullets_hit = stats.get(statname + "bullethit", 0)
        self.terrorist_hunt.melee_kills = stats.get(statname + "meleekills", 0)
        self.terrorist_hunt.bullets_fired = stats.get(statname + "bulletfired", 0)
        self.terrorist_hunt.matches_played = stats.get(statname + "matchplayed", 0)
        self.terrorist_hunt.kill_assists = stats.get(statname + "killassists", 0)
        self.terrorist_hunt.time_played = stats.get(statname + "timeplayed", 0)
        self.terrorist_hunt.revives = stats.get(statname + "revive", 0)
        self.terrorist_hunt.kills = stats.get(statname + "kills", 0)
        self.terrorist_hunt.headshots = stats.get(statname + "headshot", 0)
        self.terrorist_hunt.matches_lost = stats.get(statname + "matchlost", 0)
        self.terrorist_hunt.dbno_assists = stats.get(statname + "dbnoassists", 0)
        self.terrorist_hunt.suicides = stats.get(statname + "suicide", 0)
        self.terrorist_hunt.barricades_deployed = stats.get(statname + "barricadedeployed", 0)
        self.terrorist_hunt.reinforcements_deployed = stats.get(statname + "reinforcementdeploy", 0)
        self.terrorist_hunt.total_xp = stats.get(statname + "totalxp", 0)
        self.terrorist_hunt.rappel_breaches = stats.get(statname + "rappelbreach", 0)
        self.terrorist_hunt.distance_travelled = stats.get(statname + "distancetravelled", 0)
        self.terrorist_hunt.revives_denied = stats.get(statname + "revivedenied", 0)
        self.terrorist_hunt.dbnos = stats.get(statname + "dbno", 0)
        self.terrorist_hunt.gadgets_destroyed = stats.get(statname + "gadgetdestroy", 0)
        self.terrorist_hunt.areas_secured = stats.get(statname + "servershacked", 0)
        self.terrorist_hunt.areas_defended = stats.get(statname + "serverdefender", 0)
        self.terrorist_hunt.areas_contested = stats.get(statname + "serveraggression", 0)
        self.terrorist_hunt.hostages_rescued = stats.get(statname + "hostagerescue", 0)
        self.terrorist_hunt.hostages_defended = stats.get(statname + "hostagedefense", 0)
        self.terrorist_hunt.blind_kills = stats.get(statname + "blindkills", 0)

        return self.terrorist_hunt

    @asyncio.coroutine
    def check_terrohunt(self, data=None):
        """|coro|

        Checks the players general stats for terrorist hunt, only loading them if they haven't been loaded already"""
        if self.terrorist_hunt is None:
            yield from self.load_terrohunt(data=data)
        return self.terrorist_hunt

    @property
    def wins(self):
        return self.won

    @property
    def losses(self):
        return self.lost