class RankedRegions:
    """リージョン

    内容
    ----------
    EU : str
        EUデータ
    NA : str
        America データ
    ASIA : str
        APACデータ"""
    EU = "emea"
    NA = "ncsa"
    ASIA = "apac"


valid_regions = [x.lower() for x in dir(RankedRegions) if "_" not in x]


class Rank:
    """ランク

    内容
    ----------
    RANKS : list[str]
        ランク一覧
    RANK_CHARMS : list[str]
        ランクチャーム一覧
    UNRANKED : int
        ランクなしのID
    COPPER : int
        copperのID
    BRONZE : int
        BronzeのID
    SILVER : int
        silverのID
    GOLD : int
        goldのID
    PLATINUM : int
        platinumのID
    DIAMOND : int
        diamondのID
    max_mmr : int
        最高MMR
    mmr : int
        現在のMMR
    wins : int
        勝利数
    losses : int
        敗北数
    abandons : int
        引き分け？

    rank_id : int
        現在のランクのID
    rank : str
        現在のランクの名前
    max_rank : int
        最高ランクのID
    next_rank_mmr : int
        次のランクのMMR
    season : int
        シーズンiD
    region : str
        リージョンID
    skill_mean : float
        Skill?
    skill_stdev : float
        わからん
    """
    RANKS = ["Unranked",
             "Copper 4", "Copper 3", "Copper 2", "Copper 1",
             "Bronze 4", "Bronze 3", "Bronze 2", "Bronze 1",
             "Silver 4", "Silver 3", "Silver 2", "Silver 1",
             "Gold 4", "Gold 3", "Gold 2", "Gold 1",
             "Platinum 3", "Platinum 2", "Platinum 1", "Diamond"]

    RANK_CHARMS = [
        "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season02%20-%20copper%20charm.44c1ede2.png",
        "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season02%20-%20bronze%20charm.5edcf1c6.png",
        "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season02%20-%20silver%20charm.adde1d01.png",
        "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season02%20-%20gold%20charm.1667669d.png",
        "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season02%20-%20platinum%20charm.d7f950d5.png",
        "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season02%20-%20diamond%20charm.e66cad88.png"
    ]

    RANK_ICONS = [
        "https://i.imgur.com/sB11BIz.png",  # unranked
        "https://i.imgur.com/ehILQ3i.jpg",  # copper 4
        "https://i.imgur.com/6CxJoMn.jpg",  # copper 3
        "https://i.imgur.com/eI11lah.jpg",  # copper 2
        "https://i.imgur.com/0J0jSWB.jpg",  # copper 1
        "https://i.imgur.com/42AC7RD.jpg",  # bronze 4
        "https://i.imgur.com/QD5LYD7.jpg",  # bronze 3
        "https://i.imgur.com/9AORiNm.jpg",  # bronze 2
        "https://i.imgur.com/hmPhPBj.jpg",  # bronze 1
        "https://i.imgur.com/D36ZfuR.jpg",  # silver 4
        "https://i.imgur.com/m8GToyF.jpg",  # silver 3
        "https://i.imgur.com/EswGcx1.jpg",  # silver 2
        "https://i.imgur.com/KmFpkNc.jpg",  # silver 1
        "https://i.imgur.com/6Qg6aaH.jpg",  # gold 4
        "https://i.imgur.com/B0s1o1h.jpg",  # gold 3
        "https://i.imgur.com/ELbGMc7.jpg",  # gold 2
        "https://i.imgur.com/ffDmiPk.jpg",  # gold 1
        "https://i.imgur.com/Sv3PQQE.jpg",  # plat 3
        "https://i.imgur.com/Uq3WhzZ.jpg",  # plat 2
        "https://i.imgur.com/xx03Pc5.jpg",  # plat 1
        "https://i.imgur.com/nODE0QI.jpg"  # diamond
    ]

    @staticmethod
    def bracket_from_rank(rank_id):
        if rank_id == 0:
            return 0
        elif rank_id <= 4:
            return 1
        elif rank_id <= 8:
            return 2
        elif rank_id <= 12:
            return 3
        elif rank_id <= 16:
            return 4
        elif rank_id <= 19:
            return 5
        else:
            return 6

    @staticmethod
    def bracket_name(bracket):
        if bracket == 0:
            return "Unranked"
        elif bracket == 1:
            return "Copper"
        elif bracket == 2:
            return "Bronze"
        elif bracket == 3:
            return "Silver"
        elif bracket == 4:
            return "Gold"
        elif bracket == 5:
            return "Platinum"
        else:
            return "Diamond"

    UNRANKED = 0
    COPPER = 1
    BRONZE = 2
    SILVER = 3
    GOLD = 4
    PLATINUM = 5
    DIAMOND = 6

    def __init__(self, data):
        self.max_mmr = data.get("max_mmr")
        self.mmr = data.get("mmr")
        self.wins = data.get("wins")
        self.losses = data.get("losses")
        self.rank_id = data.get("rank", 0)
        self.rank = Rank.RANKS[self.rank_id]
        self.max_rank = data.get("max_rank")
        self.next_rank_mmr = data.get("next_rank_mmr")
        self.season = data.get("season")
        self.region = data.get("region")
        self.abandons = data.get("abandons")
        self.skill_mean = data.get("skill_mean")
        self.skill_stdev = data.get("skill_stdev")

    def get_icon_url(self):
        """Get URL for this rank's icon

        Returns
        -------
        :class:`str`
            the URL for the rank icon"""
        return self.RANK_ICONS[self.rank_id]

    def get_maxicon_url(self):
        """Get URL for this rank's icon

        Returns
        -------
        :class:`str`
            the URL for the rank icon"""
        return self.RANK_ICONS[self.max_rank]

    def get_charm_url(self):
        """Get charm URL for the bracket this rank is in

        Returns
        -------
        :class:`str`
            the URL for the charm

        """
        if self.rank_id <= 4: return self.RANK_CHARMS[0]
        if self.rank_id <= 8: return self.RANK_CHARMS[1]
        if self.rank_id <= 12: return self.RANK_CHARMS[2]
        if self.rank_id <= 16: return self.RANK_CHARMS[3]
        if self.rank_id <= 19: return self.RANK_CHARMS[4]
        return self.RANK_CHARMS[5]

    def get_bracket(self):
        """Get rank bracket

        Returns
        -------
        :class:`int`
            the id for the rank bracket this rank is in

        """
        return Rank.bracket_from_rank(self.rank_id)

    def get_max_rank(self):
        max_rank_id = Rank.bracket_from_rank(self.max_rank)
        rank_name = Rank.bracket_name(max_rank_id)
        if self.max_rank == 0:
            return rank_name
        elif self.max_rank <= 16:
            if self.max_rank % 4 == 0:
                return rank_name + " 1"
            elif self.max_rank % 4 == 1:
                return rank_name + " 4"
            elif self.max_rank % 4 == 2:
                return rank_name + ' 3'
            elif self.max_rank % 4 == 3:
                return rank_name + " 2"
            else:
                return rank_name
        elif self.max_rank <= 19:
            if self.max_rank == 17:
                return rank_name + " 3"
            elif self.max_rank == 18:
                return rank_name + " 2"
            else:
                return rank_name + " 1"
        else:
            return rank_name





