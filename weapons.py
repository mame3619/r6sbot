class WeaponTypes:
    """ウェポンタイプ

    内容
    ----------
    ASSAULT_RIFLE : int
        AR
    SUBMACHINE_GUN : int
        SMG
    MARKSMAN_RIFLE : int
        MR
    SHOTGUN : int
        SG
    HANDGUN : int
        HG
    LIGHT_MACHINE_GUN : int
        LMG
    MACHINE_PISTOL : int
        マシンピストル"""
    ASSAULT_RIFLE = 0
    SUBMACHINE_GUN = 1
    MARKSMAN_RIFLE = 2
    SHOTGUN = 3
    HANDGUN = 4
    LIGHT_MACHINE_GUN = 5
    MACHINE_PISTOL = 6


WeaponNames = [
    "Assault Rifle",
    "Submachine Gun",
    "Marksman Rifle",
    "Shotgun",
    "Handgun",
    "Light Machine Gun",
    "Machine Pistol"
]

class Weapon:
    """ウェポン

    内容
    ----------
    type : int
        ウェポンID
    name : str
        ウェポンの名前
    kills : int
        ウェポンのキル
    headshots : int
        ウェポンでのHS
    hits : int
        ウェポンでのヒット
    shots : int
        ウェポンでのショット数

    """
    def __init__(self, weaponType, stats=None):
        self.type = weaponType
        self.name = WeaponNames[self.type]
        
        stat_name = lambda name: "weapontypepvp_%s:%s:infinite" % (name, self.type)

        stats = stats or {}
        self.kills = stats.get(stat_name("kills"), 0)
        self.headshots = stats.get(stat_name("headshot"), 0)
        self.hits = stats.get(stat_name("bullethit"), 0)
        self.shots = stats.get(stat_name("bulletfired"), 0)